Setup
======

Setup is a script to set up a Mac OS X laptop for Rails development.

Requirements
------------

### Mac OS X

1) Install a C compiler.

Use [OS X GCC Installer](https://github.com/kennethreitz/osx-gcc-installer/) for
Snow Leopard (OS X 10.6).

Use [Command Line Tools for XCode](https://developer.apple.com/downloads/index.action)
for Lion (OS X 10.7) or Mountain Lion (OS X 10.8).

2) Set zsh as your login shell:

    chsh -s /bin/zsh

Install
-------

### Mac OS X

Read, then run the script:

    zsh <(curl -s https://bitbucket.org/theragollc/setup/raw/master/mac)


What it sets up
---------------

* Bundler gem for managing Ruby libraries
* Heroku Toolbelt for interacting with the Heroku API
* Homebrew for managing operating system libraries (OS X only)
* ImageMagick for cropping and resizing images
* Postgres for storing relational data
* Postgres gem for talking to Postgres from Ruby
* Rails gem for writing web applications
* RVM for managing versions of the Ruby programming language
* SSH public key for authenticating with BitBucket and Heroku
* Watch for periodically executing a program and displaying the output

It should take less than 15 minutes to install (depends on your machine).

Credits
-------

Originally based on:
![thoughtbot](http://thoughtbot.com/assets/tm/logo.png)

License
-------

Laptop is © 2013 TheraGo LLC. It is free software, and may be
redistributed under the terms specified in the LICENSE file.